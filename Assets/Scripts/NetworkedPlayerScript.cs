﻿using UnityEngine;
using UnityEngine.Networking;

public class NetworkedPlayerScript : NetworkBehaviour 
{
	//olisi voinut kirjoittaa myös using, mutta näin voi viitata suoraan assetteihin...näppärää!
	public UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController rbFpsController;
	public Camera fpsCamera;
	public UnityStandardAssets.Characters.FirstPerson.HeadBob headBob;
	public AudioListener audioListener;
	public PlayerController playerController;
	//private GunShootingScript shootingScript;
	private ShowRedFlash showRedFlash;
	public void Start(){
		showRedFlash = FindObjectOfType<ShowRedFlash> ();
	}

	public override void OnStartLocalPlayer()
	{
		Debug.Log ("NetworkedPlayer:OnStartLocalPlayer()");
		rbFpsController.enabled = true;
		headBob.enabled = true;
		fpsCamera.enabled = true;
		audioListener.enabled = true;
		//shooting script pitää kyllä itse jo sisällään tämän enabled jos localPlayer tiedon... mutta jos
		//se on puuttunut niin se on asetettu falseksi, että kai tämä voi täälläkin olla. Pitäs testaa
		//toimiiko ilman tätä - kommentti 24.1. toimii ihan ok, customnetworkmanagerin kanssa näin. tänne tullaan jos ollaan host.
		//shootingScript.enabled = true;
		playerController.enabled = true;

		gameObject.name = "Local Player";//tämä jotta näkyy paremmin editorissa kuka on kuka!

		//varmistetaa että ollaan kartalla, tähän voi tulla muutoksia kun spawnpointataan.
		//gameObject.transform.position = new Vector3(45f,6f,5f); tämä korjataan networkspawnpointeilla

		//pitää sulkea local playerin aloittaessa network-managerin hudi, jottei X:n painaminen vahingossa katkaise peliä.
		NetworkManagerHUD networkManagerHUD = FindObjectOfType<NetworkManagerHUD>();
		networkManagerHUD.showGUI = false;

		base.OnStartLocalPlayer ();
	}

	void ToggleRenderer(bool isAlive){
		Renderer[] renderers = GetComponentsInChildren<Renderer> ();

		for (int i = 0; i < renderers.Length; i++) {
			renderers [i].enabled = isAlive;
		}
	}

	void ToggleControls(bool isAlive){
		rbFpsController.enabled = isAlive;
		//shootingScript.enabled = isAlive;

		playerController.enabled = isAlive;
		fpsCamera.cullingMask = ~fpsCamera.cullingMask; //invertoi cullingMaskin everything <=> nothing , tää voi kai periaatteessa alkaa bugaa!!! jos kutsutaan väärin
	}
	//nykyisin damage = 0, tulee efekti, mutta ei kuolemaa, damage > 0 tulee myös kuolema.
	[ClientRpc]
	public void RpcResolveHit (int damage){
		//this code runs on all clients when the player is hit

		//flash red when local player is hit
		if (isLocalPlayer) {
			showRedFlash.ShowFlashEffect (0.5f);
		}

		if (damage > 0) {

			ToggleRenderer (false);

			if (isLocalPlayer) {
				//Hey mr network manager, give me a new spawn position!
				Transform spawn = NetworkManager.singleton.GetStartPosition ();
				//move while we are invisible
				transform.position = spawn.position;
				transform.rotation = spawn.rotation;

				ToggleControls (false);
			}
			//Let us respawn in 2 seconds, every client for himself!
			Invoke ("Respawn", 2f);
		}
	}

	void Respawn(){
		ToggleRenderer (true);

		if (isLocalPlayer)
			ToggleControls (true);
	}

}

