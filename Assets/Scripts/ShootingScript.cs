﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
//tämä on se vanha skripta joka korvattiin GunShootingScript:llä ja PlayerControllerilla.
public class ShootingScript : NetworkBehaviour {
	public ParticleSystem _muzzleFlash;
	public AudioSource _gunAudio;
	public GameObject _impactPrefab;
	public Transform cameraTransform;//tää vois olla joku muukin piste... mutta tää ainakin jos ammutaan ns lonkalta.
	//kohde voisi olla esim 100 numeron päässä keskellä. 

	public float maxRange = 100f;

	BulletImpact _impactEffect;

	// Use this for initialization
	void Start () {
		GameObject go = Instantiate (_impactPrefab);
		_impactEffect = go.GetComponent<BulletImpact> ();

		//estetään updateja pyörimästä jos ei olla localPlayer.
		if (!isLocalPlayer) {
			enabled = false;
		} else {
			enabled = true;
			//tallennetaan GameManageriin tieto, että localPlayer on startannut.
			GameManager gm = FindObjectOfType<GameManager>();
			gm.setLocalPlayer(gameObject);
		}

	}


	public void Suicide(){
		CmdHitPlayer (transform.position,gameObject);//Suicideaction for desperate situations or fun.
		
	}
	public void FireGun(){
		playGunEffect ();
		/* otettiin pois, katsotaan miten käy, tuleeko outoja virheitä yms hassua.*/
		RaycastHit hit;
		//ammutaan noin 1 koordinaatti oman pään edestä, ettei osuta itseensä.<- blaablaa
		//TODO tämän voisi laittaa ampumaan aseen piipusta... tosin pelaaja ei näinkään varmaan huomaa eroa.
		Vector3 rayPos = cameraTransform.position;// + (1f * cameraTransform.forward); EI TARVITA TÄTÄ SIIRTOA, raycast ei osu siihen mistä lähtee

		if(Physics.Raycast(rayPos,cameraTransform.forward, out hit, maxRange))
		{
			/*_impactEffect.transform.position = hit.point;
				_impactEffect.Play ();*/
			playImpactEffect (hit.point);
			if (hit.transform.tag == "Player") {
				CmdHitPlayer (hit.point, hit.transform.gameObject);
			} else if(hit.transform.tag == "destructable") {
				CmdHitDestructable(hit.point, hit.transform.gameObject);
			} else {
				CmdHitSomething(hit.point);
			}
		} else {
			CmdHitNothing();
		}
	}
	// Update is called once per frame
	void Update () 
	{
		
		//pelaaja ampuu näin!
		if(Input.GetButtonDown("Fire1"))
		{
			FireGun ();
		}
	}

	private void playGunEffect(){
		Debug.Log ("playGunEffect");
		_muzzleFlash.Stop ();
		_muzzleFlash.Play ();
		_gunAudio.Stop ();
		_gunAudio.Play ();
	}
	private void playImpactEffect(Vector3 impactPoint){		
		Debug.Log ("playImpactEffect");
		_impactEffect.transform.position = impactPoint;
		_impactEffect.Play ();
	}
	[ClientRpc]
	public void RpcPlayEffect (Vector3 hitPoint){
		//tulitusefekti tehdään muilla paitsi ei local playerilla. Local player tuotti sen jo.
		if (isLocalPlayer)
			return;
		playGunEffect ();
		playImpactEffect (hitPoint);
	}
	[ClientRpc]
	public void RpcPlayGunEffectOnly(){
		if (isLocalPlayer)
			return;
		playGunEffect ();
	}
	[Command]
	void CmdHitPlayer(Vector3 hitPoint,GameObject hit){
		RpcPlayEffect (hitPoint);
		int damage = 1;
		hit.GetComponent<NetworkedPlayerScript> ().RpcResolveHit (damage);
	}
	[Command]
	void CmdHitDestructable(Vector3 hitPoint, GameObject hit){
		RpcPlayEffect (hitPoint);
		Destroy (hit);
	}
	[Command]
	void CmdHitSomething(Vector3 hitPoint){
		RpcPlayEffect (hitPoint);
	}
	[Command]
	void CmdHitNothing(){
		RpcPlayGunEffectOnly ();
	}
}
