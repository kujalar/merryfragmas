﻿using UnityEngine;
using System.Collections;

public class WayPoint  {
	public bool isLooping = false;//kun tämä on kohdattu, siirretään waypointti waypointtilistassa viimeiseksi (eli se tulee uusiksi, on looping)
	public Vector3 position;
	public Quaternion rotation;
	public string formation;

	//looping can be used as a patrol route waypoint.
	public void init(Vector3 position, Quaternion rotation,bool isLooping,string formation)
	{
		this.isLooping = isLooping;
		this.position = position;
		this.rotation = rotation;
		this.formation = formation;
	}
	// Use this for initialization
	/*void Start () {
	
	}*/
	
	// Update is called once per frame
	/*void Update () {
	
	}*/
}
