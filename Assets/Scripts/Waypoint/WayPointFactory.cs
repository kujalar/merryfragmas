﻿using UnityEngine;
using System.Collections;


public class WayPointFactory : MonoBehaviour{
	public bool isLooping = false;//kun tämä on kohdattu, siirretään waypointti waypointtilistassa viimeiseksi (eli se tulee uusiksi, on looping)
	[Header("Platoon orders")]
	public string formation = Platoon.FORMATION_LINE;

	private int layerMaskGround;
	// Use this for initialization
	void Start () {
		layerMaskGround = LayerMask.GetMask ("Ground");
	}
	
	// Update is called once per frame
	/*
	void Update () {
	
	}
	*/
	public WayPoint buildWayPoint(){
		WayPoint wayPoint = new WayPoint ();
		Vector3 wayPointPosition;
		if(fixWaypointPosition (gameObject.transform.position,out wayPointPosition)) {
	//		Debug.Log ("buildWayPoint: "+gameObject.transform.position.ToString()+"->"+wayPointPosition.ToString());
		} else {
			Debug.LogWarning ("buildWayPoint - raycast ei osunut maahan - waypointti pitää ehkä nostaa? "+gameObject.transform.position.ToString()+"->"+wayPointPosition.ToString());
		}

		Quaternion rotation = gameObject.transform.rotation;
		wayPoint.init (wayPointPosition,rotation,isLooping,formation);
		return wayPoint;
	}

	/*
	 * RayCast max 50f alaspäin. Palautetaan osumapiste ground leveliin.
	 * Jos osumapistettä ei löydy, niin palautetaan false ja asetetaan fixedpointiksi se mikä fromPoint olikin.
	 * Tämä tehdään siksi, jotta waypointit eivät jäisi vahingossa roikkumaan ilmaan. 
	 */
	private bool fixWaypointPosition(Vector3 fromPoint,out Vector3 fixedPoint){
		RaycastHit hit;

		if (Physics.Raycast (fromPoint, Vector3.down, out hit, 50f, layerMaskGround)) {
			fixedPoint = hit.point;

			return true;
		}
		fixedPoint = fromPoint;
		
		return false;
	}
}
