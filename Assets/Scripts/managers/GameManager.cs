﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	private GameObject m_localPlayer;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	//tallettaa kuka on tällä clientillä lokaalipelaaja.
	public void setLocalPlayer(GameObject localPlayer){
		m_localPlayer = localPlayer;
	}
	public GameObject getLocalPlayer(){
		return m_localPlayer;
	}

	public void QuitTheGame(){
		Application.Quit ();
	}
}
