﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShowRedFlash : MonoBehaviour {
	public float flashIntensity = 0.75f;

	Image flashImage;
	float startFlash = 0f;
	float flashDuration = 0f;

	// Use this for initialization
	void Start () {
		flashImage = GetComponent<Image> ();
	}


	public void ShowFlashEffect(float duration){
		
		flashDuration += (duration-startFlash);
		startFlash = 0;
	}

	// Update is called once per frame
	//TODO tämä olisi parempi tehdä Co-routinena - koska tämä tehdään vain joskus
	void Update () {
		
		if (flashDuration > 0f) {
			float calculus = (startFlash / flashDuration);
			float alpha = Mathf.Lerp (flashIntensity, 0f, calculus);
			//Debug.Log ("fi="+flashIntensity+" calculus="+calculus);
			Color c = flashImage.color;
			c.a = alpha;
			flashImage.color = c;
			if (startFlash > flashDuration) {
				flashDuration = 0f;
				startFlash = 0f;
			} else {
				startFlash += Time.deltaTime;
			}
		}
	}
}
