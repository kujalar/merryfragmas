﻿using UnityEngine;
using System.Collections;

public class SuicideAction :MonoBehaviour {

	public void OnClick(){
		GameObject localPlayer = FindObjectOfType <GameManager> ().getLocalPlayer ();
		localPlayer.GetComponent<ShootingScript> ().Suicide ();
		GetComponentInParent<CanvasController> ().CloseQuitPanel ();
	}
}
