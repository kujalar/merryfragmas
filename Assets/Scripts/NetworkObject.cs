﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class NetworkObject : NetworkBehaviour {
	
	/*
	bool onStartClient = false;

	override public void OnStartClient(){
		Debug.Log ("NetworkObject:OnStartClient()");
		onStartClient = true;
	}*/

	// Use this for initialization
	void Start () {
	}

	public override void OnStartServer(){
		Debug.Log ("NetworkObject:OnStartServer()"); //+" onStartClient="+onStartClient);
		//tää spawnaa itsensä nettiin, ja tää luodaan vain serverillä tässä.
		//if (isServer) {
			NetworkIdentity netId = this.GetComponent<NetworkIdentity> ();
			netId.serverOnly = false;
			NetworkServer.Spawn (this.gameObject);
		/*} /*else if (!onStartClient) {
			Destroy (this);
		}*/

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
