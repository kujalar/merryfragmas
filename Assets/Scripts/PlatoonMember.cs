﻿using UnityEngine;
using System.Collections;

public class PlatoonMember : MonoBehaviour {
	[HideInInspector]
	public Platoon myPlatoon;
	[HideInInspector]
	public int myPlatoonIndex;
	//[HideInInspector]
	//public Vector3 myPositionOffset;
	[HideInInspector]
	public Vector3 platoonTargetPoint;
	[HideInInspector]
	public Quaternion platoonTargetRotation;
	[HideInInspector]
	public bool platoonTargetAchieved = false;

	public delegate void PlatoonMemberDestroyed(PlatoonMember platoonMember);
	public event PlatoonMemberDestroyed onPlatoonMemberDestroyed;
	// Use this for initialization
	void Start () {
		
	}

	public void setPlatoonTarget(Vector3 platoonTargetPoint,Quaternion platoonTargetRotation){
		this.platoonTargetPoint = platoonTargetPoint;
		this.platoonTargetRotation = platoonTargetRotation;
		platoonTargetAchieved = false;

	}

	/*
	 * palauttaa true sillä vuorolla, kun platoon target saavutetaan.
	 */
	public bool checkPlatoonTargetIsReached(){
		//merkitään platoon waypointti saavutetuksi, mikäli agentin formationi kertoo että tavoite on saavutettu 
		//tälle kyseiselle platoonMemberille.
		if (platoonTargetAchieved==false && isCloseToPlatoonTarget()) {
			onAchievedPlatoonTarget ();
			//targettia ei oteta pois, koska voidaan hetki vielä esim kävellä sitä lähemmäksi. nyt on vain merkitty, että
			//platooni tietää tyypin olevan lähellä targettiaan.
			return true;
		}
		return false;
	}
	/*
	 * tarkistaa ollaanko niin lähellä platoon targettia, että se voitaisiin merkitä saavutetuksi.
	 */
	public bool isCloseToPlatoonTarget(){
		return myPlatoon.getFormation ().isFormationTargetReached (this);
	}

	public void onAchievedPlatoonTarget (){
		platoonTargetAchieved = true;
		//tämä voisi kertoa platoon luokalle, että yksi jäsenistä saavutti kohteen, ja sitten joukkue voi katsoa täytyykö triggeröidä lisää.
		myPlatoon.onMemberAchievedPlatoonTarget();

	}
	// Update is called once per frame
/*	void Update () {
	
	}*/
	void OnDestroy(){
		if (onPlatoonMemberDestroyed != null) {
			onPlatoonMemberDestroyed (this);
		}
	}
}
