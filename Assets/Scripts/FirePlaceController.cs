﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class FirePlaceController : NetworkBehaviour {
	public Light flameLight;
	public GameObject actionPanel; //näyttää (x) for action, ei kuitekaan nykyisellään ota inputtia vastaan, vaan tämä objecti!
	public bool intensityChanges;

	private GameObject activeLocalPlayer;
	private ParticleSystem takkatuli;
	private bool xForAction = false;
	private Text actionText;

	private float changeSpeed = 0.1f;
	private float oldIntensity = 1f;
	private float newIntensity = 1f;
	private float lerpCounter = 0f;

//	private delegate void ToggleFlameAction();
//	private event ToggleFlameAction OnToggleFlame;
	// Use this for initialization
	void Start () {
		takkatuli = GetComponent<ParticleSystem> ();
		actionText = actionPanel.GetComponentInChildren<Text>();
		//Tästä on tarkoitus tehdä pelaajan toiminnolla käynnistettävä, mutta nyt se käynnistyy startista.
		//IgniteFlame ();
		activeLocalPlayer = null;
	}

	public void IgniteFlame(){
		if (flameLight.enabled == true) {
			return;//jos liekit jo leiskuu, ei sytytys tee mitään.
		}
		takkatuli.Stop ();
		takkatuli.Play ();
		flameLight.enabled = true;
		StartCoroutine (StartFlameIntensityLoop());
		if (xForAction) {
			updateActionPanelText ();
		}
	}
	public void ExtinguishFlame(){
		if (flameLight.enabled == false) {
			return;//jos on jo sammuksissa ei liekkiin pissaus sammuta enempää.
		}
		takkatuli.Stop();
		flameLight.enabled = false;
		if (xForAction) {
			updateActionPanelText ();
		}
	}

	IEnumerator StartFlameIntensityLoop()
	{
		while(flameLight.enabled)
		{
			changeSpeed = Random.Range (0.1f, 0.5f);
			oldIntensity = flameLight.intensity;
			newIntensity = Random.Range (1.0f, 2.5f);
			lerpCounter = 0f;
			yield return new WaitForSeconds(changeSpeed);
		}
	}

	void OnTriggerEnter(Collider other){
		if (isGameObjectLocalPlayer (other.gameObject)) {
			//Debug.Log ("You enter. Press X to action.");
			//IgniteFlame ();
			xForAction =  true;
			updateActionPanelText ();
			actionPanel.SetActive (true);
			//saadaan setattua helpoiten täällä, kun ekan kerran tarvitaan , sitten voi jättää muistiin.
			activeLocalPlayer = other.gameObject;
		}

	}
	private void updateActionPanelText(){
		if (flameLight.enabled) {
			actionText.text = "(X) Sammuta tuli.";
		} else {
			actionText.text = "(X) Sytytä tuli.";
		}

	}
	void OnTriggerExit(Collider other){
		if (isGameObjectLocalPlayer (other.gameObject)) {
			//Debug.Log ("You left the fireplace.");
			//ExtinguishFlame ();
			xForAction = false;
			actionPanel.SetActive (false);
		}
	}
	private bool isGameObjectLocalPlayer(GameObject gameObject){
		if (gameObject.CompareTag ("Player")) {
			NetworkedPlayerScript netWorkPlayer = gameObject.GetComponent<NetworkedPlayerScript> ();
			if (netWorkPlayer.isLocalPlayer) {
				return true;
			}
		}
		return false;
	}
	private void toggleFlame(){
		if (flameLight.enabled == false) {
			
			//komentoja ei voi kutsua turvasyistä suoraan, ne pitää kutsua pelaajan kautta
			//CmdIgniteThis();

			MFCommandCtrl cmdCtrl = activeLocalPlayer.GetComponent<MFCommandCtrl> ();
			cmdCtrl.igniteTakka (gameObject);
		} else {
			
			//komentoja ei voi kutsua turvasyista suoraan, ne pitää kutsua pelaajan kautta
			//CmdExtinguishThis();

			MFCommandCtrl cmdCtrl = activeLocalPlayer.GetComponent <MFCommandCtrl> ();
			cmdCtrl.extinguishTakka (gameObject);
		}
	}

	[ClientRpc]
	public void RpcIgniteThis(){
		IgniteFlame ();
	}
	[ClientRpc]
	public void RpcExtinguishThis(){
		ExtinguishFlame ();
	}
	//laitoin nämä server salasanalle, kun minusta nämä vaatii sen ihan toiminnanohjaukseksi. 
	//6.2.2016 en ole vielä testannut toimiiko kun tallennan ja menen aamiaiselle.
	[Server]
	public void IgniteThis(){
		RpcIgniteThis ();
	}
	[Server]
	public void ExtinguishThis(){
		RpcExtinguishThis ();
	}

	// Update is called once per frame
	void Update () {

		if (xForAction) {
			if(Input.GetKeyDown(KeyCode.X)){
				toggleFlame();
			}
		}

		if (intensityChanges) {
			lerpCounter += Time.deltaTime;
			float lerpValue = lerpCounter / changeSpeed;
			flameLight.intensity = Mathf.Lerp (oldIntensity, newIntensity, lerpValue);
		}
	}
}
