﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BtnKeyListener : MonoBehaviour {
	public string keyCodeName;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(keyCodeName)) {
			GetComponent<Button> ().onClick.Invoke();
		}
	}
}
