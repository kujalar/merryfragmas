﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class MFCommandCtrl : NetworkBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void igniteTakka(GameObject takka){
		CmdIgniteTakka (takka);
	}

	public void extinguishTakka(GameObject takka){
		CmdExtinguishTakka (takka);
	}

	[Command]
	void CmdIgniteTakka(GameObject takka){
		FirePlaceController fpc = takka.GetComponent<FirePlaceController> ();
		fpc.IgniteThis (); //tässä ehkä vähän turha mennä yhden funktion kautta, mutta katsotaan mihin tämä suunnittelu johtaa...
	}
	[Command]
	void CmdExtinguishTakka(GameObject takka){
		FirePlaceController fpc = takka.GetComponent<FirePlaceController> ();
		fpc.ExtinguishThis ();
	}

}
