﻿using UnityEngine;
using System.Collections;

public class BulletImpact : MonoBehaviour {

	private ParticleSystem _particleSystem;
	private AudioSource _audioSource;
	// Use this for initialization
	void Start () {
		_particleSystem = GetComponent<ParticleSystem> ();
		_audioSource = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	/*void Update () {
	
	}*/
	public void Play()
	{
		_particleSystem.Stop ();
		_particleSystem.Play ();

		_audioSource.Stop ();
		_audioSource.Play ();
	}
}
