﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class NetworkedEnemyScript : NetworkBehaviour {

	NavMeshAgent navigationAgent;
	BotBrain botBrainScript;
	// Use this for initialization
	void Start () {
		
	}

	public override void OnStartServer(){
		navigationAgent = GetComponent<NavMeshAgent> ();
		botBrainScript = GetComponent<BotBrain> ();
		BotSight botSightScript = GetComponent<BotSight> ();

		navigationAgent.enabled = true;
		botBrainScript.enabled = true;
		botSightScript.enabled = true;

		NetworkIdentity netId = this.GetComponent<NetworkIdentity> ();
		netId.serverOnly = false;
		NetworkServer.Spawn (this.gameObject);

		base.OnStartServer ();
	}

	// Update is called once per frame
	void Update () {
	
	}
}
