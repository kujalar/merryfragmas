﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Platoon : MonoBehaviour {
	public string platoonName;
	//initialisointiin
	public PlatoonMember[] initPlatoonMembers;


	private List<PlatoonMember> platoonMembers = new List<PlatoonMember>();
	private Formation formation; //tee tästä formation interface
	private BotWayPointScript platoonWayPointScript;
	private WayPoint platoonTarget;
	// Use this for initialization

	public static string FORMATION_LINE = "line";
	public static string FORMATION_ROW = "row";
	public static string FORMATION_MOB = "mob";

	//formation stores
	private Formation formation_line;
	private Formation formation_row; 
	private Formation formation_mob;

	void Start () {
		platoonWayPointScript = GetComponent<BotWayPointScript> ();
		platoonTarget = null;

		formation_line = new LineFormation (transform);
		formation_row = new RowFormation (this);
		formation_mob = new MobFormation (this);
		formation = formation_line;//defaulta

		if (initPlatoonMembers != null ) {
			for (int i = 0; i < initPlatoonMembers.Length; i++) {
				AddPlatoonMember (initPlatoonMembers [i] );
			}
		}

	}
	private void refreshPlatoonIndexes (){
		/* tää varmaan hoitaisi hyvin, kokeillaan ensin luultavasti tehokkaampaa ratkaisua
		foreach (PlatoonMember platoonMember in platoonMembers) {
			platoonMember.myPlatoonIndex = platoonMembers.IndexOf (platoonMember);
		}
		*/
		for (int i = 0; i < platoonMembers.Count; i++) {
			PlatoonMember platoonMember = platoonMembers [i];
			if (platoonMember != null) {
				platoonMember.myPlatoonIndex = i;
			}
		}
	}
	/*
	 * tätä kutsutaan PlatoonMember Skriptasta silloin kun hän kuolee.
	 */
	public void OnPlatoonMemberDestroyed(PlatoonMember platoonMember){
		Debug.Log ("OnPlatoonMemberDestroyed");
		//poistetaan tätä kuunteleva eventti.
		platoonMember.onPlatoonMemberDestroyed -= OnPlatoonMemberDestroyed;
		//poistetaan platoonMember platoon listalta.
		platoonMembers.Remove(platoonMember);
		//päivitetään listan jäsenten "indeksit"
		refreshPlatoonIndexes();

		//jos tällä platoon memberillä ei ollut platoon target achieved, ja kaikilla muilla oli, niin platooni
		//on saattanut saavuttaa targetin. tehdään sama temppu kuin "tuhoutunut objekti olisi saavuttanut platoon targetin. (koska sen
		//platoon target saavuttamattomuus ei enää 'häritse' muuta platoonia)
		if (platoonMember.platoonTargetAchieved == false) {
			onMemberAchievedPlatoonTarget ();
		}
	}

	/*
	 * palauttaa platoonin nykyisen formationin.
	 */
	public Formation getFormation(){
		return formation;
	}
	public List<PlatoonMember> getPlatoonMembers(){
		return platoonMembers;
	}
	public void AddPlatoonMember(PlatoonMember platoonMember){
		platoonMembers.Add (platoonMember);
		platoonMember.myPlatoon = this;
		platoonMember.myPlatoonIndex = platoonMembers.IndexOf (platoonMember);

		//subscripataan platoonMemberin eventti onPlatoonMemberDestroyed();
		//muista, että siellä missä platoonMember poistetaan platoon listalta, on nämä OnPlatoonMember referenssit myös poistettava.
		//samaten jos tämä luokka tuhoutuu, on nämä poistettava, mutta toistaiseksi (2016-03-05) ainoa miten platoon member katoaa platoonista, on jos
		//sen gameobjekti destroyataan.
		platoonMember.onPlatoonMemberDestroyed += OnPlatoonMemberDestroyed;
	}

	public void issuePlatoonTargetToMembers(){
		for (int i = 0; i < platoonMembers.Count; i++) {
			PlatoonMember member = platoonMembers [i];
			Vector3 membersTarget = formation.getPositionInWorldSpace(platoonTarget.position,member);
			member.setPlatoonTarget (membersTarget,platoonTarget.rotation);
		}
	}
	private bool isPlatoonReachedCurrentWayPoint(){
		for (int i = 0; i < platoonMembers.Count; i++) {
			PlatoonMember member = platoonMembers [i];
			if (member.platoonTargetAchieved == false) {
				return false;
			}
		}
		return true;
	}

	public void onMemberAchievedPlatoonTarget(){
		//Debug.Log ("somebody reached platoon target");
		if (isPlatoonReachedCurrentWayPoint ()) {
			platoonWayPointScript.AchieveNextUnAchievedWaypoint ();

			//TODO , jos platoonilta otetaan pois update cyklestä waypointin asetuksen tarkastus, niin
			//sitten tähän kohtaan voidaan lisätä waypointin asetanta.
			platoonTarget = null;
		}

	}

	// Update is called once per frame
	void Update () {
		//tarkista onko platoon waypoint saavutettu? ja jos on vaihda seuraava
		//TODO


		//tarkista onko platoon waypoint asetettu?
		if (platoonTarget == null) {
			platoonTarget = platoonWayPointScript.getNextUnAchievedWaypoint ();
			if (platoonTarget != null) {
				//TODO tässä voisi olla formation selector-itemi jos semmoisen tekisi
				if (FORMATION_LINE.Equals (platoonTarget.formation)) {
					formation = formation_line;
				} else if (FORMATION_ROW.Equals (platoonTarget.formation)) {  
					formation = formation_row;
				} else {
					//defaultti
					formation = formation_mob;
				}
				//issue platoon waypoint command to all platoon members.
				issuePlatoonTargetToMembers();
			}
		}


	}
}
public interface Formation {
	Vector3 getPositionInWorldSpace (Vector3 platoonPosition, PlatoonMember member);
	//TODO siirrä formation metodiksi se, missä tarkistetaan onko joku platoonMember saavuttanut formationtargettinsa.
	//nykyisin koodi löytyy BotBrain skriptan riviltä 70, mutta se pitää saada formatiokohtaiseksi, jotta voidaan lennossa
	//vaihdella kävelevän pelaajan targettia, sekä kertoa milloin hän on ns. ready eli saavuttanut waypointin.
	bool isFormationTargetReached(PlatoonMember member);
}
public class LineFormation : Formation {
	public float valinEtaisyys = 2f;
	public Transform platoonTransform;

	public LineFormation(Transform platoonTransform){
		this.platoonTransform = platoonTransform;
	}
	public bool isFormationTargetReached(PlatoonMember member){
		if (member.platoonTargetPoint == null) {
			return false;
		}
		//rivi muodostelmassa jäsenelle on annettu oma platoonTargetPoint ja kun hän on saapunut 2 mittayksikön päähän tästä, 
		//on hän saavuttanut kohteensa.
		float distance = (member.transform.position - member.platoonTargetPoint).magnitude;
		if (distance < 2f) {//jos ollaan alle 3 päässä joukkueen kohteesta, ollaan lähellä
			return true;
		}
		return false;
	}
	/*
	 * palauttaa worldSpace koordinaation platoon sijainnilla tässä formationissa tietyn platoon indexin hepulle.
	 */
	public Vector3 getPositionInWorldSpace(Vector3 platoonPosition, PlatoonMember member){
		Vector3 positionOffset = getPositionOffset (member.myPlatoonIndex);
		return platoonPosition + positionOffset;
	}

	public Vector3 getPositionOffset(int platoonIndex){
		if (platoonIndex == 0) {
			return Vector3.zero;
		}
		Vector3 left = platoonTransform.right.normalized*(-1);
		return left * valinEtaisyys * platoonIndex;
	}
}

public class RowFormation : Formation {
	public float valinEtaisyys = 2f;
	private Platoon platoon;

	public RowFormation(Platoon platoon){
		this.platoon = platoon;
	}

	public Vector3 getPositionInWorldSpace(Vector3 platoonPosition,PlatoonMember member){
		if (member.myPlatoonIndex == 0) {
			return platoonPosition;
		}
		return platoonPosition;
	}
	//TODO nyt tämä on vielä kuten line formationissa, jotta saadaan koodi kääntymään, mutta tarkoitus on muuttaa logiikkaa.
	//TODO tee tästä semmoinen että porukka osaa pysähtyä kun etummainen
	//päätyy formationin targettiin ja ilmoittaa olevansa ns ready.
	//muista jättää tyyppien väliin spacea ehkä 2f ja lisäksi muista laittaa että seuraava
	//seuraa aina edellistään.
	public bool isFormationTargetReached(PlatoonMember member){
		if (member.platoonTargetPoint == null) {
			return false;
		}
		List<PlatoonMember> platoonMembers = platoon.getPlatoonMembers ();
		//päivitetään platoon targetti tietylle jonossa astelevalle herralle, ellei hän ole herra indexillä 0 eli jonon eka.
		if(member.myPlatoonIndex > 0 ) {
			member.platoonTargetPoint =  platoonMembers[member.myPlatoonIndex - 1].transform.position;
		}

		float distance = (member.transform.position - member.platoonTargetPoint).magnitude;

		if (distance < 4f && member.myPlatoonIndex > 0) {
			//TODO tähän voisi tehdä stop-komennonkin. nyt kokeillaan näin...
			member.platoonTargetPoint = member.transform.position;
			//jos edellinen platoonmember on jo saavuttanut kohteen ja itse ollaan alle 2 päässä hänestä ollaan itsekkin saavutettu ja pysähdytään
			if (platoonMembers [member.myPlatoonIndex - 1].platoonTargetAchieved == true) {
				return true;
			}
		} 

		if (member.myPlatoonIndex == 0 && distance < 2f) {
			return true;
		}

		return false;
	}
}

public class MobFormation : Formation {
	public float valinEtaisyys = 2f;
	private Platoon platoon;

	public MobFormation(Platoon platoon){
		this.platoon = platoon;
	}
	public bool isFormationTargetReached(PlatoonMember member){
		if (member.platoonTargetPoint == null) {
			return false;
		}
		//mob - eli väkijoukko muodostelmassa kaikille jäsenille on annettu target pointti, ja kun he ovat 4 mittayksikön päästä siitä ovat he sen saavuttaneet. 
		float distance = (member.transform.position - member.platoonTargetPoint).magnitude;
		if (distance < 4f) {//jos ollaan alle 4 päässä joukkueen kohteesta, ollaan lähellä
			return true;
		}
		return false;
	}
	public Vector3 getPositionInWorldSpace(Vector3 platoonPosition,PlatoonMember member){
		if (member.myPlatoonIndex == 0) {
			return platoonPosition;
		}
		return platoonPosition;
	}
}