﻿using UnityEngine;
using System.Collections;

public class CanvasController : MonoBehaviour {
	public GameObject _quitPanel;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey ("escape")) {
			_quitPanel.SetActive (true);
		}
	}

	public void CloseQuitPanel(){
		_quitPanel.SetActive(false);
	}
}
