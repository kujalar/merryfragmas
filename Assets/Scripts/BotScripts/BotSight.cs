﻿using UnityEngine;
using System.Collections;

public class BotSight : MonoBehaviour {

//	public Transform debugObject;
//	public Transform botEye;
	public Camera myCamera;//tää pitää sit aina olla!

	private CharacterRegistry characterRegistry;
	private RegisteredCharacter currentRegisteredCharacter;
	private int losCheckIndex = 0;
	// Use this for initialization
	void Start () {
		//character registry tarvitaan hoitamaan spottausta round-robin tyyppisesti.
		//samalla updatella ei pidä yrittää tarkastaa kaikkia mahdollisia targetteja, 
		//muuten framePerSecond menee nopeasti pöntöstä.
		characterRegistry = FindObjectOfType<CharacterRegistry> ();
		currentRegisteredCharacter = GetComponent<RegisteredCharacter> ();
		if (myCamera == null) {
			Debug.LogWarning ("BotSight: kamera puuttuu! muistaa asentaa myCamera!");
		}
	}
	/*
	public float AngleDir(Vector3 fwd, Vector3 toTarget, Vector3 axisDirection){
		Vector3 perp = Vector3.Cross (fwd, toTarget);
		float dir = Vector3.Dot (perp, axisDirection);
		return dir;
	}

	public float UpDownAngle(Vector3 offsetToDestination,float destinationY,float originY){
		
		float distance = offsetToDestination.magnitude;
		float diffIny = destinationY - originY;
		float kulmaPelaajanKorkeudesta = 0;
		if (distance != 0 && diffIny != 0) {
			kulmaPelaajanKorkeudesta = Mathf.Asin (diffIny/distance);
		}
		kulmaPelaajanKorkeudesta = kulmaPelaajanKorkeudesta * Mathf.Rad2Deg;
		float katsomiskulma = Mathf.Asin (botEye.forward.normalized.y) * Mathf.Rad2Deg;
		float kulmaKatseeseen = katsomiskulma - kulmaPelaajanKorkeudesta;
		//Debug.Log ("kulmaKatseesta="+kulmaKatseesta+" kulmaPelaajanKorkeudesta="+kulmaPelaajanKorkeudesta+" katsomiskulma="+katsomiskulma);
		return kulmaKatseeseen;
	}

	public Vector3 ProjectPointOnPlane(Vector3 planeNormal, Vector3 planePoint, Vector3 point){
		planeNormal.Normalize ();
		float distance = -Vector3.Dot (planeNormal.normalized, (point-planePoint));

		return point + planeNormal * distance;
	}*/

	private bool IMightSeePointInMyCamera(Vector3 targetPoint){
		if (myCamera == null) {
			Debug.LogWarning ("IMightSeePointInMyCamera does not have myCamera setup.");
			return false;
		}
		Vector3 screenPoint = myCamera.WorldToViewportPoint (targetPoint);

		if (screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1) {
			return true;
		}
		return false;
	}


	/*
	private bool ICanSee(GameObject target){
		Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera);
		if (GeometryUtility.TestPlanesAABB (planes, target.collider.bounds)) {<-- tähän on olemassa joku uudempi juttu
			return true;
		} else {
			return false;
		}
	}
	*/

	private bool CheckIfInFieldOfView(Transform targetTransform){

		//Debug.Log ("angleFromPlane="+angleFromPlane);
		bool imightsee = IMightSeePointInMyCamera(targetTransform.position);

		//Debug.Log ("mightsee="+imightsee.ToString()+" characterRegistry.getSize()="+characterRegistry.getSize());

		if (imightsee == false) {
			return false;
		}

		//tee tänne raycastit targettiin.

		return true;
	}

	//Tee tästä siistimpi metodi - jota voi kutsua useasti tuota CheckIfHasLOS metodista.- jätit kesken että voi katsoa olipakerranin.
	private bool RaycastHitsTarget(Vector3 rayStart, Vector3 targetPoint, GameObject opponent){
		RaycastHit hit;
		Vector3 rayDirection = targetPoint - rayStart;
		if (Physics.Raycast (rayStart, rayDirection, out hit, rayDirection.magnitude)) {
			if (hit.transform.gameObject == opponent) {
				return true;
			} else {
				//Debug.Log ("CheckIfHaslos - hit something - ei lossia.");
			}
		} else {
			//Debug.Log ("CheckIfHasLos - hit nothing - hyvin outoa?");//tämä on outoa koska aina pitäisi vähintään osua kohteeseen.
		}
		return false;
	}

	private bool CheckIfHasLOS(RegisteredCharacter opponentCharacter){
		
		Transform opponentCameraTransform = opponentCharacter.getCameraTransform ();


		//ammutaan noin 1 koordinaatti oman pään edestä, ettei osuta itseensä.
		//TODO tämän voisi laittaa ampumaan aseen piipusta... tosin pelaaja ei näinkään varmaan huomaa eroa.
		//Vector3 rayPos = cameraTransform.position + (1f * cameraTransform.forward); <- ei tarvis varmaan noin siirtää... colliderin sisältä voi lähteä ohjekirjan mukaan.
		Vector3 rayStart = myCamera.transform.position;
		if (RaycastHitsTarget (rayStart, opponentCameraTransform.position, opponentCharacter.transform.gameObject)) {
			return true;
		}


		//ehkä tehdään lisää tarkastuksia jos cameratransformiin ei voinut osua! nyt ei ole implementoitu niitä enempää


		return false;

	}

	public bool CheckLosToNextOpponent(out RegisteredCharacter opponent){
		bool opponentSeen = false;
		int nextLosCheckIndex = 0;
		if (characterRegistry == null) {
			opponent = null;
			return false;
		}
		opponent = characterRegistry.getNextOpponentCharacter (losCheckIndex,out nextLosCheckIndex, currentRegisteredCharacter);

		if (opponent != null) {
			opponentSeen = CheckIfInFieldOfView(opponent.gameObject.transform);
			if (opponentSeen) {
				opponentSeen = CheckIfHasLOS (opponent);
			}
		}

		losCheckIndex = nextLosCheckIndex;
		return opponentSeen;
	}


	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Z)&&characterRegistry.getSize()>0) {
			//lasketaan testiarvot kohti harmaata vihollista
			/*
			bool opponentSeen = false;
			int nextLosCheckIndex = 0;
			RegisteredCharacter opponentCharacter = characterRegistry.getNextOpponentCharacter (losCheckIndex,out nextLosCheckIndex, currentRegisteredCharacter);

			if (opponentCharacter != null) {
				opponentSeen = CheckIfInFieldOfView(opponentCharacter.gameObject.transform);
				if (opponentSeen) {
					opponentSeen = CheckIfHasLOS (opponentCharacter);
				}
			}
			*/
			RegisteredCharacter opponentChecked = null;
			int currentLosCheckIndex = losCheckIndex;
			bool opponentSeen = CheckLosToNextOpponent (out opponentChecked);

			//Debug.Log ("opponentSeen="+opponentSeen+" currentCheckIndex="+currentLosCheckIndex+" nextCheckIndex="+losCheckIndex);

		}
	}
}
