﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

[System.Serializable]
public class GunParams {
	public float minReloadTime = 2f;
	public float maxReloadTime = 3f;
}

public class GunShootingScript : NetworkBehaviour {
	public ParticleSystem _muzzleFlash;
	public AudioSource _gunAudio;
	public GameObject _impactPrefab;
	public Transform fireTransform;

	public bool noCasualties = false;
	public float maxRange = 100f;
	public GunParams gunParams;

	float timeSinceLastShot = 0f;
	float currentReloadTime = 0f;
	BulletImpact _impactEffect;
	GameObject impactGameObject;
	// Use this for initialization
	void Start () {
		impactGameObject = Instantiate (_impactPrefab);
		_impactEffect = impactGameObject.GetComponent<BulletImpact> ();

		//estetään updateja pyörimästä jos ei olla localPlayer.
		if (!isLocalPlayer) {
			enabled = false;
		} else {
			enabled = true;
			//tallennetaan GameManageriin tieto, että localPlayer on startannut.
			GameManager gm = FindObjectOfType<GameManager>();
			gm.setLocalPlayer(gameObject);
		}

	}
	void OnDestroy(){
		//Debug ("GunShooting Script - OnDestroy");
		if (impactGameObject != null) {
			//Debug ("Destroy _impactEffect");
			Destroy (impactGameObject);
		}
	}

	public void Suicide(){
		CmdHitPlayer (transform.position,gameObject);//Suicideaction for desperate situations or fun.

	}
	/*
	 * tämä metodi otettiin irralleen, jotta botit voivat tarkistaa tulilinjan ennen kuin ampuvat.
	 * 
	 */
	public bool checkHit(out RaycastHit hit){
		Vector3 rayPos = fireTransform.position;
		bool result = Physics.Raycast (rayPos, fireTransform.forward, out hit, maxRange);
		return result;
	}
	public void FireGun(){
		//tässä on kehittämistä vielä lataamisen suhteen....
		timeSinceLastShot += Time.deltaTime;
		//jos latausaika on kesken ei voi ampua, 
		if (timeSinceLastShot < currentReloadTime) {
			return;
		}

		playGunEffect ();
		/* otettiin pois, katsotaan miten käy, tuleeko outoja virheitä yms hassua.*/
		RaycastHit hit;
		//ammutaan noin 1 koordinaatti oman pään edestä, ettei osuta itseensä.<- blaablaa
		//TODO tämän voisi laittaa ampumaan aseen piipusta... tosin pelaaja ei näinkään varmaan huomaa eroa.
		//Vector3 rayPos = fireTransform.position;// + (1f * cameraTransform.forward); EI TARVITA TÄTÄ SIIRTOA, raycast ei osu siihen mistä lähtee

		//jos tehdään aseen tähtäysrutiini niin tämän fireTransform.forwardin voi muuttaa....
		//if(Physics.Raycast(rayPos,fireTransform.forward, out hit, maxRange))
		if(checkHit(out hit))
		{
			playImpactEffect (hit.point);
			//Debug.Log ("Raycast hit tag "+hit.transform.tag);
			if (hit.transform.tag == "Player") {

				CmdHitPlayer (hit.point, hit.transform.gameObject);
			} else if(hit.transform.tag == "destructable") {
				//TODO seuraa näiden rataa ja kerro serverille myöhemmin, että pitää tuhota.

				CmdHitDestructable(hit.point, hit.transform.gameObject);
			} else {
				CmdHitSomething(hit.point);
			}
		} else {
			CmdHitNothing();
		}

		//asetetaan laukaus tapahtuneeksi.... ja nyt alkaa reload time,
		//TODO voidaan tehdä joku "clip size" eli lippaan koko, ettei tarvi ladata ihan koko ajana
		timeSinceLastShot = 0f;
		currentReloadTime = Random.Range (gunParams.minReloadTime, gunParams.maxReloadTime);
	}

	// Update is called once per frame
	void Update () 
	{

		/* pelaaja kutsuu näin!!! ei botti
		if(Input.GetButtonDown("Fire1"))
		{
			FireGun ();
		}*/
	}

	private void playGunEffect(){
		//Debug.Log ("playGunEffect");
		_muzzleFlash.Stop ();
		_muzzleFlash.Play ();
		_gunAudio.Stop ();
		_gunAudio.Play ();
	}
	private void playImpactEffect(Vector3 impactPoint){		
		//Debug.Log ("playImpactEffect");
		_impactEffect.transform.position = impactPoint;
		_impactEffect.Play ();
	}
	[ClientRpc]
	public void RpcPlayEffect (Vector3 hitPoint){
		//tulitusefekti tehdään muilla paitsi ei local playerilla. Local player tuotti sen jo.
		if (isLocalPlayer)
			return;
		playGunEffect ();
		playImpactEffect (hitPoint);
	}
	[ClientRpc]
	public void RpcPlayGunEffectOnly(){
		if (isLocalPlayer)
			return;
		playGunEffect ();
	}
	[Command]
	void CmdHitPlayer(Vector3 hitPoint,GameObject hit){
		RpcPlayEffect (hitPoint);
		int damage = 1;
		if (noCasualties) {
			damage = 0;
		}
		hit.GetComponent<NetworkedPlayerScript> ().RpcResolveHit (damage);
	}
	[Command]
	void CmdHitDestructable(Vector3 hitPoint, GameObject hit){
		RpcPlayEffect (hitPoint);
		if(!noCasualties)
			Destroy (hit);
	}
	[Command]
	void CmdHitSomething(Vector3 hitPoint){
		RpcPlayEffect (hitPoint);
	}
	[Command]
	void CmdHitNothing(){
		RpcPlayGunEffectOnly ();
	}
}
