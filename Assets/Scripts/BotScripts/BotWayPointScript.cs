﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BotWayPointScript : MonoBehaviour {
	//initialisointiin
	public WayPointFactory[] initWaypoints;


	private List<WayPoint> waypoints = new List<WayPoint>();
	private WayPoint lastAchievedWayPoint = null;
	// Use this for initialization
	void Start () {
		if (initWaypoints != null ) {
			for (int i = 0; i < initWaypoints.Length; i++) {
				AddWaypoint (initWaypoints [i].buildWayPoint()  );
			}
		}
	}

	public void AddWaypoint(WayPoint waypoint){
		waypoints.Add (waypoint);
	}

	public void RemoveWaypoint(WayPoint waypoint){
		waypoints.Remove (waypoint);
	}
	public WayPoint getNextUnAchievedWaypoint(){
		if (waypoints.Count < 1) {
			return null;
		}
		return waypoints[0];
	}
	public void AchieveNextUnAchievedWaypoint(){
		//poistetaan non-looping waypoint.
		WayPoint achievedWaypoint = getNextUnAchievedWaypoint();

		//mielestäni c#:ssa last achieved waypointin pitäisi joutua roskankeruuseen kun sen referenssi korvataan uudella. aivan kuten javassakin.
		//looping ei poistu koska siihen on yhä olemassa referenssi waypoints - listasta.

		lastAchievedWayPoint = achievedWaypoint;
		waypoints.RemoveAt(0);
		if (achievedWaypoint.isLooping) {
			//looping waypoint siirretään waypoint listassa viimeiseksi, jolloin se looppaa uudestaan kohteeksi kun aika koittaa
			waypoints.Add (achievedWaypoint);
		}
		//
	}
	public WayPoint getLastAchievedWaypoint(){
		return lastAchievedWayPoint;
	}
	// Update is called once per frame
	/*void Update () {
	
	}*/

}
