﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ShootingSettings {
	public bool fireAtWill = true;
	public float minReactionTime = 0.1f;
	public float maxReactionTime = 0.2f;

}
	


public class BotBrain : MonoBehaviour {
	public GunShootingScript botShootingScript;
	public BotSight botSight;
	[Header("Spotting settings")]
	public float doNotLoseRange = 3.0f;
	[Header("Movement settings")]
	public float stopDistanceWayPoint = 1f;
	public float reachDistanceWayPoint = 2f;
	public float stopDistanceSpottedTarget = 3f;
	public bool navigateToSpottedTarget = true;
/*	[Header("Shooting settings")]
	public bool fireAtWill = true;
	public float reloadTime = 3f;//3 sekunnin latausaika laukausten välissä.*/
	public ShootingSettings shootingSettings;

	private BotWayPointScript wayPointScript;
	private PlatoonMember platoonMemberScript;

	private RegisteredCharacter myOwnRegisteredCharacter;
	NavMeshAgent agent;
	RegisteredCharacter spottedTarget = null;
	private bool navigationTargetIsValid = false;
	private Vector3 navigationTarget;
	private Vector3 lastVisibleSpottedTarget;
	private bool mayNavigateToLastSpotted = false;

	private List<StuckCheckParam> platoonMembers = new List<StuckCheckParam>();

	private GunShootingScript gunShooting;
	// Use this for initialization
	void Start () {
		agent = GetComponent< NavMeshAgent >();
		gunShooting = GetComponent<GunShootingScript> ();
		myOwnRegisteredCharacter = GetComponent<RegisteredCharacter> ();
		wayPointScript = GetComponent<BotWayPointScript> ();
		platoonMemberScript = GetComponent<PlatoonMember> ();

		//käynnistetään CoRoutine joka tarkistaa kerran sekunnissa, missä tyyppi on. jos kaikki viimeisen 3 sekunnin sijainnin on lähekkäin ja
		//tyypin pitäisi liikkua, niin silloin nostetaan hälytys. Jos kaikki 6 sekunnin aikaiset sijainnit ovat lähekkäin nostetaan erikoishälytys.

		StartCoroutine (CheckIfStuck());
	}

	private float stuckCheckPeriod = 1.0f;
	private float stuckRange = 0.1f;
	private int stuckWarningValue = 3;
	private int stuckCounter = 0;
	private Vector3 lastLocation = null;
	private bool doStuckCheck = false;
	private void startStuckCheck(){
		stuckCounter = 0;
		doStuckCheck = true;
		StartCoroutine (CheckIfStuck());
	}
	private void stopStuckCheck(){
		doStuckCheck = false;
	}
	/*
	 * TODO tämä ei taida tarvita synkronointia, mutta sen voisi testata kyllä, että mitä tapahtuu jos sama objekti kutsuu coroutinea käyntiin
	 * monesti, eli pitääkö se jotenkin estää.
	 * 
	 */
	IEnumerator CheckIfStuck(){
		while (doStuckCheck) {
			if (lastLocation != null) {
				//TODO jos tarvitaan kevyt algoritmi liikkeen laskemiseksi, voidaan esim 3 sekunnin välein ottaa muistiin missä tyyppi oli, ja verrata aina siihen 3 sek välein 
				//kokonaisliikkeen määrä! TÄÄ OIS HYVÄ IDEA, sijainti tarvitaan vain jos ollaan saatu jumitustulos perusalgoritmista.
				float move = (lastLocation - transform.position).magnitude;
				//jos move on pienempi kuin jokin raja-arvo ja jumittumista täytyy tarkkailla,
				//lisätään counteria. Tarkkaillaan jumia nyt ekassa versiossa kokoajan.
				//Tarvitaanko jumittumisen laskemiseksi historiatietoa missä tyyppi oli esim 3 sekuntia sitten?
				if (move <= stuckRange) {
					stuckCounter++;
					if (stuckCounter >= stuckWarningValue) {
						Debug.LogWarning ("WAROITUS! joku hyyppä on jumissa!");
					}
				} else {
					stuckCounter = 0;
				}
			}
			lastLocation = transform.position;
			yield return new WaitForSeconds (stuckCheckPeriod);
		}
	}
	/*
	private Transform selectSomePlayerFromPlay(){
		NetworkedPlayerScript nwPlayerScript = FindObjectOfType<NetworkedPlayerScript> ();
		if(nwPlayerScript!=null){
			return nwPlayerScript.transform;
		} else {
			return null;
		}
	}*/

	IEnumerator Rotate(Quaternion targetDir){
		
		float speed = 120f;
		bool continueTurning = true;
		while(continueTurning){
			//transform.rotation = Quaternion.Slerp(fromRotation, toRotation, Time.time * speed);
			float step = speed * 0.1f;
			Quaternion newDir = Quaternion.RotateTowards(transform.rotation, targetDir, step);
			transform.rotation = newDir;
			if(Quaternion.Angle(targetDir,newDir)<=0.1f){
				continueTurning = false;
			}
			yield return new WaitForSeconds (0.1f);
		}
	}

	private void NavigateToPlatoonTarget(){
		if (platoonMemberScript == null || platoonMemberScript.myPlatoon == null ) {
			return;
		}
		if (navigateToSpottedTarget && ( spottedTarget != null || mayNavigateToLastSpotted == true )) {
			//jos navigateToSpottedTarget on päällä ja on olemassa spottedTarget, ei navigoida platoontargetille.
			return;
		}

		//jos platoon target on olemassa, ja on saavuttu tänne, ja etäisyys platoon targettiin on pidempi kuin
		// 1f niin kävellään normaalisti targettia kohti.
		if (platoonMemberScript.platoonTargetPoint != null   //&& !platoonMemberScript.platoonTargetAchieved
			&& ! ( Vector3.SqrMagnitude(platoonMemberScript.platoonTargetPoint - agent.destination)<1f )
			//&& !navigationTarget.Equals(agent.destination)
			) {
			agent.stoppingDistance = 0.5f;
			agent.SetDestination (platoonMemberScript.platoonTargetPoint);


			agent.updateRotation = true;

		}



		if (platoonMemberScript.checkPlatoonTargetIsReached ()) {
			//tullaan tänne sillä vuorolla kun platoon target havaitaan saavutetuksi.
			//kytketään navigation agentin rotaatio pois päältä
			agent.updateRotation = false;
			StartCoroutine (Rotate (platoonMemberScript.platoonTargetRotation));
		} else if (platoonMemberScript.platoonTargetAchieved && agent.updateRotation) {
			//tullaan tänne, mikäli rotatio on laitettu päälle, vaikka platoon target on saavutettu (voi mennä päälle esim jos spotataan vihu ja
			//lähtään sen perään...
			//täällä sitten katsotaan, kun palataan takas platoon targettia kohti, että kuuluu updaterotate ottaa pois päältä
			//ja laittaa agentti kääntymään taas ns oikeaan suuntaan automaagisesti.
			if (platoonMemberScript.isCloseToPlatoonTarget ()) {
				agent.updateRotation = false;
				StartCoroutine (Rotate (platoonMemberScript.platoonTargetRotation));
			
			}
		} 

	}
	/*
	* Jos ei ole spottedTargettia ja navigate To Spotted target, niin silloin navigaatiokohde on waypoint.
	*/
	private void NavigateToWayPoint(){
		if (wayPointScript == null) {
			return;
		}
		//pitää tarkistaa ollaanko riittävän lähellä waypointtia, jotta se voitaisiin vaihtaa toiseksi.
		//jos on olemassa seuraava waypointti, ja ollaan siitä niin lähellä, että se on saavutettu,
		//voidaan seuraava waypointti merkitä saavutetuksi, jonka jälkeen yritetään hakea uutta waypointtia,
		//mikäli semmoisia vielä on.
		WayPoint nextWaypoint = wayPointScript.getNextUnAchievedWaypoint ();
		if (nextWaypoint != null) {
			if ((nextWaypoint.position - gameObject.transform.position).magnitude < reachDistanceWayPoint) {
				wayPointScript.AchieveNextUnAchievedWaypoint ();
				nextWaypoint = wayPointScript.getNextUnAchievedWaypoint ();
			}
		}



		if (navigateToSpottedTarget && spottedTarget != null ) {
			//jos navigateToSpottedTarget on päällä ja on olemassa spottedTarget, ei navigoida waypointille.
			return;
		}


		if (nextWaypoint != null) {
			//Debug.Log ("wp="+nextWaypoint.position);
			if (navigationTarget == null || !navigationTarget.Equals (nextWaypoint.position)) {
				navigationTarget = nextWaypoint.position;
				agent.stoppingDistance = stopDistanceWayPoint;
				agent.SetDestination (navigationTarget);
			}
		}
	}
	/*
	 * jos on "navigateToSpottedTarge=true ja on olemassa spottedTarget, niin
	 * sen sijainti asetetaan agent (NavMeshAgent) muuttujan targetiksi.
	 */
	private void NavigateToSpottedTarget(){
		if (!navigateToSpottedTarget) {
			return;
		}

		if (spottedTarget != null) {
			navigationTarget = spottedTarget.transform.position;
			navigationTargetIsValid = true;
			agent.stoppingDistance = stopDistanceSpottedTarget;
			agent.SetDestination (navigationTarget);
			agent.updateRotation = true;
		} else if (mayNavigateToLastSpotted == true && navigationTarget != lastVisibleSpottedTarget) {
			navigationTarget = lastVisibleSpottedTarget;
			navigationTargetIsValid = true;
			agent.stoppingDistance = stopDistanceSpottedTarget;
			agent.SetDestination (navigationTarget);
			agent.updateRotation = true;
		} else if (mayNavigateToLastSpotted == true) {
			//jos etäisyys viimeksi nähdylle spotted targetille on tarpeeksi lyhyt, otetaan mayNavigateToSpottedTarget falseksi.
			if ((lastVisibleSpottedTarget - transform.position).magnitude <= stopDistanceSpottedTarget) {
				mayNavigateToLastSpotted = false;
			}
		}
	}


	/*
	 * jos on fireAtWill niin ampuu kun juoksee pelaajan perässä.
	 * 
	 */
	private void FiringDecision(){
		
		if (!shootingSettings.fireAtWill||spottedTarget==null) {
			return;
		}

		//TODO Optimointivinkki, jos tässä on jo raycastattu, ei ampumisfunktion tarvi castata uusiksi, tän castauksen tulos voidaan antaa sinne!!!
		RaycastHit hit;
		//pitää tsekata ettei omia ole linjalla niin voidaan tulittaa
		if (gunShooting.checkHit (out hit)) {
			//Debug.Log ("hit.transfrom.tag="+hit.transform.tag);
			RegisteredCharacter regChar = hit.transform.gameObject.GetComponent<RegisteredCharacter> ();
			if (regChar != null) {
				//Debug.Log("regChar kohteena!");
				if (regChar.faction != null && regChar.faction.Equals (myOwnRegisteredCharacter.faction)) {
					//ei ammuta omia...
					//Debug.Log("ampuminen on peruttu. ei ammuta omia!");
					return;
				}
			}
		}

		//voidaan ampua uudestaan jos latausaika ei ole kesken ja tekee mieli ampua!

		gunShooting.FireGun ();



	}
	/*
	 * aktivoi spottausrundia, spotataan 1 targetti 
	 */
	private void SpotTarget(){
		RegisteredCharacter opponent = null;

		if (botSight.CheckLosToNextOpponent (out opponent)) {
			if (spottedTarget == null) {
				//emme ole vielä nähneet yhtäkään vihollista, joten eka joka nähdään on automaagisesti spotattu
				spottedTarget = opponent;
				//spotted target on asetettu tyhjiltään joten hän on lastVisibleSpottedTarget
				lastVisibleSpottedTarget = spottedTarget.transform.position;
				//tässä voidaan katkaista last spotted targetin etsiminen
				mayNavigateToLastSpotted = false;
			} else if (spottedTarget != opponent) {
				//näemme eri vihollisen ja maali voi vaihtua... mutta ei pakosti

				//laske etäisyys spotattuun targettiin ja laske etäisyys uuteen. jos uusi on pienempi, vaihda targettia.
				float squaredDistToTarget = (spottedTarget.transform.position - transform.position).sqrMagnitude;
				float squaredDistToOpponent = (opponent.transform.position - transform.position).sqrMagnitude;
				if (squaredDistToOpponent < squaredDistToTarget) {
					spottedTarget = opponent;
					//spotted target vaihtui ja on näkyvissä
					lastVisibleSpottedTarget = spottedTarget.transform.position;
				}
			} else if (spottedTarget == opponent) {
				//näemme yhä saman vanhan vihollisen...
				lastVisibleSpottedTarget = spottedTarget.transform.position;
			}

		} else if(opponent!=null && opponent==spottedTarget) {
			//jos vastustaja on alle doNotLoseRangen ei sitä hukata.
			if (doNotLoseRange < Vector3.Distance (opponent.transform.position, transform.position)) {
				
				//hukattiin havaittu vastustaja, sitä ei näy enää! vastustaja oli kauempana kuin doNotLoseRange
				spottedTarget = null;

				mayNavigateToLastSpotted = true;
				
			} else {
				//Debug.Log ("Kohdetta ei hukatta koska se on niin lähellä");
			}
		}

	}

	// Update is called once per frame
	void Update () {
		SpotTarget ();
		NavigateToPlatoonTarget ();
		NavigateToWayPoint ();
		NavigateToSpottedTarget ();
		FiringDecision ();
	}
}
