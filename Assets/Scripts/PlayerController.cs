﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	private GunShootingScript shootingScript;
	// Use this for initialization
	void Start () {
		shootingScript = GetComponent<GunShootingScript> ();
	}
	
	// Update is called once per frame
	void Update () {
		//pelaaja ampuu näin!
		if(Input.GetButtonDown("Fire1"))
		{
			shootingScript.FireGun ();
		}
	}
}
