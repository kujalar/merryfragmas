﻿using UnityEngine;
using System.Collections;

public class RegisteredCharacter : MonoBehaviour {
	public string faction = null;

	private Camera myCamera = null;
	// Use this for initialization
	void Start () {
		CharacterRegistry characterRegistry = FindObjectOfType<CharacterRegistry> ();
		RegisteredCharacter regCharacter = GetComponent<RegisteredCharacter> ();
		characterRegistry.RegisterCharacter (regCharacter);

		myCamera = GetComponentInChildren<Camera> ();
	}

	void OnDestroy(){
		CharacterRegistry characterRegistry = FindObjectOfType<CharacterRegistry> ();
		RegisteredCharacter regCharacter = GetComponent<RegisteredCharacter> ();
		if (characterRegistry != null) {
			characterRegistry.UnregisterCharacter (regCharacter);
		}
	}

	public Transform getCameraTransform(){
		if (myCamera == null) {
			return null;
		} 
		return myCamera.transform;
	}

}
