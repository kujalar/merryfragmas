﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterRegistry : MonoBehaviour {

	public static string FACTION_PLAYER = null;
	public static string FACTION_ENEMY = "enemy";

	private List<RegisteredCharacter> characters = new List<RegisteredCharacter>();


	/*
	 * Tarkoitettu käytettäväksi siten, että serveri rekisteröi kaikki pelihahmot listaan.
	 * Serveri pyörittää peliä ja voi esim ohjata bottien tekoälyä , botsight esim. tämän 
	 * rekisteröidyn character listan avulla.
	 * 
	 */
	public void RegisterCharacter(RegisteredCharacter character){
		characters.Add (character);
	}

	public void UnregisterCharacter(RegisteredCharacter character){
		characters.Remove (character);
	}

	public int getSize(){
		return characters.Count;
	}
	/*
	 * pitää palauttaa seuraava objekti, joka on eri kuin caller.
	 * jos skrollataan koko lista läpi eikä löydy muita kun caller/saman factionin tyyppejä pitää palauttaa null ja nextIndex = 0.
	 *
	 */
	public RegisteredCharacter getNextOpponentCharacter(int index, out int nextIndex, RegisteredCharacter caller){
		RegisteredCharacter candidateCharacter = null;
		//GameObject callerGameObject = caller.gameObject;
		int hatajarru = 0;
		int startIndex = index;
		bool jatkaEtsintaa = true;
		nextIndex = index;
		while (jatkaEtsintaa) {
			candidateCharacter = getCharacter (index,out nextIndex);
			index = nextIndex;

			//kelpaako candidate?
			//jos palautui null, ei ole olemassa rekisteröityjä charactereita.
			if (candidateCharacter == null) {
				jatkaEtsintaa = false;
				return null;
			}

			//jos objekti ei ole sama objekti kuin caller, ja se kuuluu eri factioniin kuin caller, se kelpaa
			if(caller!=candidateCharacter && 
				( caller.faction==null || !caller.faction.Equals(candidateCharacter.faction )))
			{
				jatkaEtsintaa = false;
				return candidateCharacter;
			}

			//jos on tehty täysi luuppi ja palattu takaisin lähtöindeksiin löytämättä yhtäkään hahmoa, joka kelpaa
			//palautetaan nulli. Tällaisia tilanteita varten voisi algoritmia ehkä optimoida, mutta toisaalta
			//liian aikainen optimointi on kaiken pahan alku. silmukka lienee silti nopea käydä läpi ja vain harvoin ei mitään havaittavaa
			//oikeasti löydy.
			if (startIndex == index) {
				candidateCharacter = null;
				jatkaEtsintaa = false;
				return null;//vähän redudanttia koodia , mutta tulee selväksi mikä on tarkoitus. tätä voi siistiä.
			}

			hatajarru++;
			if (hatajarru > 10000) {
				Debug.LogWarning ("getNextOpponentCharacter, hatajarru > 10000");
				jatkaEtsintaa = false;
			}
		}
		return candidateCharacter;
	}
	/*
	 * Tämä metodi palauttaa rekisteröityjä charactereja. Palauttaa myös seuraavan characterin indexin.
	 * Jos seuraavaa ei ole, palauttaa seuraavan indeksina jälleen listan ekan eli indexin 0. 
	 *
	 */
	private RegisteredCharacter getCharacter(int index, out int nextIndex){
		if (characters.Count <= index) {
			index = 0;
			if (characters.Count == 0) {
				nextIndex = 0;
				return null;
			}
		}
		RegisteredCharacter character = characters [index];
		index++;
		if (characters.Count <= index) {
			index = 0;
		}
		nextIndex = index;
		return character;
	}
}
